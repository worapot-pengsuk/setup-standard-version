# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.3.0](https://bitbucket.org/worapot-pengsuk/setup-standard-version/compare/version/1.3.0-alpha.1%0Dversion/1.3.0) (2020-07-03)

## [1.3.0-alpha.1](https://bitbucket.org/worapot-pengsuk/setup-standard-version/compare/version/1.3.0-alpha.0%0Dversion/1.3.0-alpha.1) (2020-07-03)


### Features

* add feature 4 ([59dd1b3](https://bitbucket.org/worapot-pengsuk/setup-standard-version/commits/59dd1b3fc5619b94c491cc5b8b11fb056039d6e6))

## [1.3.0-alpha.0](https://bitbucket.org/worapot-pengsuk/setup-standard-version/compare/version/1.2.1-alpha.1%0Dversion/1.3.0-alpha.0) (2020-07-03)


### Features

* **Feature4:** add new ! ([ad2664c](https://bitbucket.org/worapot-pengsuk/setup-standard-version/commits/ad2664ceb201e153766153fceeb847f028c8583b))

### [1.2.1-alpha.1](https://bitbucket.org/worapot-pengsuk/setup-standard-version/compare/version/1.2.1-alpha.0%0Dversion/1.2.1-alpha.1) (2020-07-03)

### [1.2.1-alpha.0](https://bitbucket.org/worapot-pengsuk/setup-standard-version/compare/version/1.2.0%0Dversion/1.2.1-alpha.0) (2020-07-03)


### Bug Fixes

* remove test [CPI-303] ([7e7acba](https://bitbucket.org/worapot-pengsuk/setup-standard-version/commits/7e7acbac53e83960d1dd16b66dd87cb6c61cd062))

## [1.2.0](https://bitbucket.org/worapot-pengsuk/setup-standard-version/compare/version/1.2.0-alpha.1%0Dversion/1.2.0) (2020-07-03)

## [1.2.0-alpha.1](https://bitbucket.org/worapot-pengsuk/setup-standard-version/compare/version/1.2.0-alpha.0%0Dversion/1.2.0-alpha.1) (2020-07-03)


### Bug Fixes

* **Feature 1:** add missing text ([01d3360](https://bitbucket.org/worapot-pengsuk/setup-standard-version/commits/01d33608a981c05f480441dc30b1f9bfcd73ffa4))

## [1.2.0-alpha.0](https://bitbucket.org/worapot-pengsuk/setup-standard-version/compare/version/1.1.1%0Dversion/1.2.0-alpha.0) (2020-07-03)


### Features

* **Feature2:** add new feature ([d16c687](https://bitbucket.org/worapot-pengsuk/setup-standard-version/commits/d16c687c35c70841d470103df1907ae40d8e12b8))

### [1.1.1](https://bitbucket.org/worapot-pengsuk/setup-standard-version/compare/version/1.1.0%0Dversion/1.1.1) (2020-07-03)

## [1.1.0](https://bitbucket.org/worapot-pengsuk/setup-standard-version/compare/version/1.1.0-alpha.6%0Dversion/1.1.0) (2020-07-03)

## 1.1.0-alpha.6 (2020-07-03)


### Features

* add feature 1 ([dbb2fac](https://bitbucket.org/worapot-pengsuk/setup-standard-version/commits/dbb2fac3db9807f4c8326b0ab8fe13cf6d718dae))


### Others

* **release:** 1.1.0-alpha.0 ([1d16d21](https://bitbucket.org/worapot-pengsuk/setup-standard-version/commits/1d16d21e1e6d838afa9d2d533f6702680438288f))
* **release:** 1.1.0-alpha.1 ([4c80fa3](https://bitbucket.org/worapot-pengsuk/setup-standard-version/commits/4c80fa3040a362b7746c0260683331bbfa8cc12b))
* **release:** 1.1.0-alpha.2 ([3da68de](https://bitbucket.org/worapot-pengsuk/setup-standard-version/commits/3da68debb30862dde26af45fe8fbedcae27de95c))
* **release:** 1.1.0-alpha.3 ([2d6f867](https://bitbucket.org/worapot-pengsuk/setup-standard-version/commits/2d6f86746cef4638cd8e0b76e6bdf9fa45849d81))
* **release:** 1.1.0-alpha.4 ([8b1c918](https://bitbucket.org/worapot-pengsuk/setup-standard-version/commits/8b1c918cda39731270fb2047c0010fa5d1a71a64))
* **release:** 1.1.0-alpha.5 ([f63f679](https://bitbucket.org/worapot-pengsuk/setup-standard-version/commits/f63f679b3580eb1a9b3013a0c43638476c5280e3))


### CI

* add versionrc ([9a787cc](https://bitbucket.org/worapot-pengsuk/setup-standard-version/commits/9a787cc5768ce1ada35a0cd1b5cc676841aa2e19))

## [1.1.0-alpha.5](https://bitbucket.org/worapot-pengsuk/setup-standard-version/compare/v1.1.0-alpha.4...v1.1.0-alpha.5) (2020-07-03)

## [1.1.0-alpha.4](https://bitbucket.org/worapot-pengsuk/setup-standard-version/compare/v1.1.0-alpha.3...v1.1.0-alpha.4) (2020-07-03)

## [1.1.0-alpha.3](https://bitbucket.org/worapot-pengsuk/setup-standard-version/compare/v1.1.0-alpha.2...v1.1.0-alpha.3) (2020-07-03)

## [1.1.0-alpha.2](https://bitbucket.org/worapot-pengsuk/setup-standard-version/compare/v1.1.0-alpha.1...v1.1.0-alpha.2) (2020-07-03)

## [1.1.0-alpha.1](https://bitbucket.org/worapot-pengsuk/setup-standard-version/compare/v1.1.0-alpha.0...v1.1.0-alpha.1) (2020-07-03)

## 1.1.0-alpha.0 (2020-07-03)


### Features

* add feature 1 ([dbb2fac](https://bitbucket.org/worapot-pengsuk/setup-standard-version/commit/dbb2fac3db9807f4c8326b0ab8fe13cf6d718dae))
